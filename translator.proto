// Protocol buffer definitions for COVIS-2 translator system
syntax = "proto3";

package translator;

// Angles are specified in integer milli-degrees
message Rotation {
    // Rotation around the X axis
    int32   roll = 1;
    // Rotation around the Y axis
    int32   tilt = 2;
    // Rotation around the Z axis
    int32   pan = 3;
    // Contains a set bit for each rotator that is moving
    // bit 0 = roll
    // bit 1 = tilt
    // bit 2 = pan
    uint32 inmotion = 5;
    // Contains a set bit for each rotator that has stalled
    // bit 0 = roll
    // bit 1 = tilt
    // bit 2 = pan
    uint32 stalled = 6;
}

message Orientation {
    // Gravity referenced angle about the X axis
    int32   roll = 1;
    // Gravity referenced angle about the Y axis
    int32   pitch = 2;
    // Magnetic heading
    int32   heading = 3;
}

// Rotator control settings
message Control {
    // Maximum rotator speed in milli-degrees/s
    int32 speed = 1;
    // Acceleration in mdeg/s**2
    int32 accel = 2;
    // Brake setting (no units, see ROS manual)
    int32 brake = 3;
}

message State {
    Rotation        r = 1;
    Orientation     o = 2;
}

message Empty {
}

service Translator {
    // Returns the current rotator reading
    rpc GetRotation(Empty) returns (Rotation) {}
    // Returns the current attitude sensor reading
    rpc GetAttitude(Empty) returns (Orientation) {}
    // Returns the current rotator and attitude sensor reading
    rpc GetState(Empty) returns (State) {}
    // Update the rotator control settings
    rpc SetControl(Control) returns (Control) {}
    // Make a relative orientation adjustment, stream State while moving
    rpc Move(Orientation) returns (stream State) {}
    // Stop all rotators
    rpc AllStop(Empty) returns (State) {}
    // Level the platform, stream State while moving
    rpc Level(Empty) returns (stream State) {}
    // Move to an absolute Rotator position
    rpc Goto(Rotation) returns (stream State) {}
}
