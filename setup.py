# -*- coding: utf-8 -*-
import sys
import os
import imp
from setuptools import setup, find_packages


# Add the current directory to the module search path.
sys.path.insert(0, os.path.abspath('.'))

# Constants
CODE_DIRECTORY = 'pycovis'
DOCS_DIRECTORY = 'docs'
TESTS_DIRECTORY = 'tests'
PYTEST_FLAGS = ['--doctest-modules']

metadata = imp.load_source(
    'metadata', os.path.join(CODE_DIRECTORY, 'metadata.py'))

entry_points = '''
[console_scripts]
rostest=pycovis.apps.rostest:cli
tcmtest=pycovis.apps.tcmtest:cli
rotatorsvc=pycovis.apps.rotatorsvc:main
'''

setup_dict = dict(
    name=metadata.package,
    version=metadata.version,
    author=metadata.authors[0],
    author_email=metadata.emails[0],
    maintainer=metadata.authors[0],
    maintainer_email=metadata.emails[0],
    url=metadata.url,
    description=metadata.description,
    packages=find_packages(exclude=('tests', 'examples')),
    install_requires=[
        'Click',
        'pyserial',
        'cmd2',
        'construct==2.5.3',
        'grpcio'
    ],
    setup_requires=[],
    # Allow tests to be run with `python setup.py test'.
    tests_require=[
        'mock'
        'pytest>=2.5.1',
        'flake8>=2.1.0',
    ],
    zip_safe=False,  # don't use eggs
    entry_points=entry_points
)


def main():
    setup(**setup_dict)


if __name__ == '__main__':
    main()
