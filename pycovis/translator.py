#!/usr/bin/env python
#
# The Translator encapsulates a three axis Rotator and an Attitude Sensor.
#
import sys
import logging
import threading
import time
import grpc
from functools import reduce
from concurrent import futures
from . import translator_pb2 as api
from . import translator_pb2_grpc as svc


rootLogger = logging.getLogger()
rootLogger.setLevel(logging.NOTSET)
logger = logging.getLogger('server')


class NoSensorError(Exception):
    def __str__(self):
        return "No attitude sensor available"


def setup_logging(level=logging.DEBUG):
    handler = logging.StreamHandler(stream=sys.stderr)
    handler.setLevel(level)
    handler.setFormatter(logging.Formatter('[%(levelname)-.1s] %(asctime)s | %(message)s'))
    rootLogger.addHandler(handler)


class TranslatorServicer(svc.TranslatorServicer):
    """
    Implement a gRPC service to provide access to the COVIS rotators and
    attitude (orientation) sensor.
    """
    # Scale factor for angle values
    scale_factor = 1000
    # Convert orientation roll angle change to rotator roll angle change
    roll2roll = 6.0
    # Convert orientation pitch angle change to rotator tilt angle change
    pitch2tilt = -1.0
    # Convert orientation heading angle change to rotator pan angle change
    hdg2pan = 1.0

    def __init__(self, pan, tilt, roll, sensor):
        """
        Initialize an object instance
        """
        self.pan = pan
        self.tilt = tilt
        self.roll = roll
        self.sensor = sensor
        self.rmutex = threading.Lock()
        self.smutex = threading.Lock()
        self.speed = 1
        self.pan.maxvel = self.speed
        self.tilt.maxvel = self.speed
        self.roll.maxvel = self.speed

    def readall(self, flushdata=False):
        s = api.State()
        with self.rmutex:
            angles = [self.roll.angle, self.tilt.angle, self.pan.angle]
            flags = [self.roll.flags, self.tilt.flags, self.pan.flags]

        s.r.roll = int(angles[0] * self.scale_factor)
        s.r.tilt = int(angles[1] * self.scale_factor)
        s.r.pan = int(angles[2] * self.scale_factor)
        s.r.inmotion = reduce(lambda a, b: a + b,
                              [(f & 0x01) << i for i, f in enumerate(flags)])
        s.r.stalled = reduce(lambda a, b: a + b,
                             [((f & 0x02) >> 1) << i
                              for i, f in enumerate(flags)])
        if self.sensor:
            with self.smutex:
                if flushdata:
                    self.sensor.kSetAcqParams(polling=0,
                                flush=1,
                                acq_interval=0,
                                resp_interval=0)
                try:
                    result = self.sensor.kGetData()
                except Exception:
                    result = self.sensor.kGetData()
                if flushdata:
                    self.sensor.kSetAcqParams(polling=0,
                                flush=0,
                                acq_interval=0,
                                resp_interval=0)
            s.o.pitch = int(result.payload.kPAngle * self.scale_factor)
            s.o.roll = int(result.payload.kRAngle * self.scale_factor)
            s.o.heading = int(result.payload.kHeading * self.scale_factor)
        return s

    def get_control(self):
        c = api.Control()
        with self.rmutex:
            # All rotators use the same control settings
            c.speed = int(self.roll.maxvel * self.scale_factor)
            c.accel = int(self.roll.accel * self.scale_factor)
            c.brake = self.roll.brake
        return c

    def GetRotation(self, req, ctx):
        logger.debug('GetRotation(%r)', req)
        r = api.Rotation()
        try:
            with self.rmutex:
                angles = [self.roll.angle, self.tilt.angle, self.pan.angle]
                flags = [self.roll.flags, self.tilt.flags, self.pan.flags]
        except Exception as e:
            ctx.set_code(grpc.StatusCode.INTERNAL)
            ctx.set_details(str(e))
        else:
            r.roll = int(angles[0] * self.scale_factor)
            r.tilt = int(angles[1] * self.scale_factor)
            r.pan = int(angles[2] * self.scale_factor)
            r.inmotion = reduce(lambda a, b: a + b,
                                [(f & 0x01) << i for i, f in enumerate(flags)])
            r.stalled = reduce(lambda a, b: a + b,
                               [((f & 0x02) >> 1) << i
                                for i, f in enumerate(flags)])
        return r

    def GetAttitude(self, req, ctx):
        logger.debug('GetAttitude(%r)', req)
        o = api.Orientation()
        try:
            if self.sensor is None:
                raise NoSensorError
            with self.smutex:
                result = self.sensor.kGetData()
        except NoSensorError as e:
            ctx.set_code(grpc.StatusCode.UNAVAILABLE)
            ctx.set_details(str(e))
        except Exception as e:
            logger.exception('GetAttitude')
            ctx.set_code(grpc.StatusCode.INTERNAL)
            ctx.set_details(str(e))
        else:
            o.pitch = int(result.payload.kPAngle * self.scale_factor)
            o.roll = int(result.payload.kRAngle * self.scale_factor)
            o.heading = int(result.payload.kHeading * self.scale_factor)
        return o

    def GetState(self, req, ctx):
        logger.debug('GetState(%r)', req)
        s = api.State()
        try:
            s = self.readall(flushdata=True)
        except Exception as e:
            logger.exception('GetState')
            ctx.set_code(grpc.StatusCode.INTERNAL)
            ctx.set_details(str(e))
        return s

    def SetControl(self, req, ctx):
        logger.debug('SetControl(%r)', req)
        c = req
        try:
            with self.rmutex:
                if req.speed > 0:
                    speed = float(req.speed)/self.scale_factor
                    self.roll.maxvel = speed
                    self.tilt.maxvel = speed
                    self.pan.maxvel = speed
                if req.accel > 0:
                    accel = float(req.accel)/self.scale_factor
                    self.roll.accel = accel
                    self.tilt.accel = accel
                    self.pan.accel = accel
                if req.brake > 0:
                    self.roll.brake = req.brake
                    self.tilt.brake = req.brake
                    self.pan.brake = req.brake
            c = self.get_control()
        except Exception as e:
            logger.exception('SetControl')
            ctx.set_code(grpc.StatusCode.INTERNAL)
            ctx.set_details(str(e))
        return c

    def AllStop(self, req, ctx):
        logger.debug('AllStop(%r)', req)
        try:
            with self.rmutex:
                self.roll.stop()
                self.tilt.stop()
                self.pan.stop()
            s = self.readall()
        except Exception as e:
            logger.exception('AllStop')
            ctx.set_code(grpc.StatusCode.INTERNAL)
            ctx.set_details(str(e))
        return s

    def Level(self, req, ctx):
        logger.debug('Level(%r)', req)
        try:
            if self.sensor is None:
                raise NoSensorError
            s = self.readall(flushdata=True)
            with self.rmutex:
                t = 0
                if s.o.roll != 0:
                    dr = -s.o.roll * self.roll2roll
                    t = max(self.roll.step(dr/self.scale_factor, self.speed), t)
                if s.o.pitch != 0:
                    dt = -s.o.pitch * self.pitch2tilt
                    t = max(self.tilt.step(dt/self.scale_factor, self.speed), t)
                t_limit = time.time() + t
            s = self.readall()
            while time.time() < t_limit and s.r.inmotion != 0:
                yield s
                time.sleep(0.5)
                s = self.readall()
        except Exception as e:
            logger.exception('Level')
            ctx.set_code(grpc.StatusCode.INTERNAL)
            ctx.set_details(str(e))

    def Move(self, req, ctx):
        logger.debug('Move(%r)', req)
        try:
            with self.rmutex:
                t = 0
                if req.roll != 0 and abs(req.roll) > 200:
                    dr = req.roll * self.roll2roll
                    t = max(self.roll.step(dr/self.scale_factor, self.speed), t)
                if req.pitch != 0 and abs(req.pitch) > 200:
                    dt = req.pitch * self.pitch2tilt
                    t = max(self.tilt.step(dt/self.scale_factor, self.speed), t)
                if req.heading != 0 and abs(req.heading) > 200:
                    dp = req.heading * self.hdg2pan
                    t = max(self.pan.step(dp/self.scale_factor, self.speed), t)
                t_limit = time.time() + t
            s = self.readall(flushdata=True)
            while time.time() < t_limit and s.r.inmotion != 0:
                yield s
                time.sleep(0.5)
                s = self.readall()
        except ValueError as e:
            logger.exception('Move')
            ctx.set_code(grpc.StatusCode.OUT_OF_RANGE)
            ctx.set_details(str(e))
        except Exception as e:
            logger.exception('Move')
            ctx.set_code(grpc.StatusCode.INTERNAL)
            ctx.set_details(str(e))

    def Goto(self, req, ctx):
        logger.debug('Goto(%r)', req)
        try:
            with self.rmutex:
                t = 0
                if req.roll > 0:
                    t = max(self.roll.stepto(req.roll/self.scale_factor, self.speed), t)
                if req.tilt > 0:
                    t = max(self.tilt.stepto(req.tilt/self.scale_factor, self.speed), t)
                if req.pan > 0:
                    t = max(self.pan.stepto(req.pan/self.scale_factor, self.speed), t)
                t_limit = time.time() + t
            s = self.readall(flushdata=True)
            while time.time() < t_limit and s.r.inmotion != 0:
                yield s
                time.sleep(0.5)
                s = self.readall()
        except ValueError as e:
            logger.exception('Goto')
            ctx.set_code(grpc.StatusCode.OUT_OF_RANGE)
            ctx.set_details(str(e))
        except Exception as e:
            logger.exception('Goto')
            ctx.set_code(grpc.StatusCode.INTERNAL)
            ctx.set_details(str(e))


def create_server(pan, tilt, roll, sensor, address):
    """
    Create a new gRPC server to listen on the specified address.

    @param pan: pan rotator
    @type pan: ros.Rotator
    @param tilt: tilt rotator
    @type tilt: ros.Rotator
    @param roll: roll rotator
    @type roll: ros.Rotator
    @param sensor: TCM orientation sensor
    @type sensor: tcm.Tcm6
    @param address: server address HOST:PORT
    @type address: string
    """
    server = grpc.server(futures.ThreadPoolExecutor(max_workers=5))
    svc.add_TranslatorServicer_to_server(
        TranslatorServicer(pan, tilt, roll, sensor), server)
    server.add_insecure_port(address)
    return server
