# -*- coding: utf-8 -*-
"""Project metadata

Information describing the project.
"""

# The package name, which is also the "UNIX name" for the project.
package = 'pycovis'
project = "COVIS-2"
project_no_spaces = project.replace(' ', '')
version = '0.9.9'
description = 'Software for the Cabled Observatory Imaging Sonar System'
authors = ['Michael Kenney']
authors_string = ', '.join(authors)
emails = ['mikek@apl.uw.edu']
license = 'MIT'
copyright = '2017 ' + authors_string
url = ''
