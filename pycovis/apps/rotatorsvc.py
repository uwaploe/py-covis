#!/usr/bin/env python
# -*- coding: utf-8 -*-
import collections
import signal
import logging
import click
import serial
import toml
from mock import Mock
from pycovis.translator import create_server, setup_logging
from pycovis.ros import Rotator
from pycovis.tcm import Tcm6


DEFAULT = """
address = "[::]:10130"
[rotators.roll]
device = "/dev/ttyMI10"
baud = 9600
[rotators.tilt]
device = "/dev/ttyMI8"
baud = 9600
[rotators.pan]
device = "/dev/ttyMI9"
baud = 9600
"""


def mock_rotator():
    rot = Mock()
    rot.moveto = Mock(return_value=2.5)
    rot.move = Mock(return_value=2.5)
    rot.stepto = Mock(return_value=2.5)
    rot.step = Mock(return_value=2.5)
    rot.reset_counter = Mock()
    rot.counter = 0
    rot.maxvel = 5
    rot.accel = 2
    rot.angle = -1
    rot.flags = 0
    rot.brake = 80
    rot.ccw = 100
    rot.cw = 900
    rot.is_moving = False
    rot.has_stalled = False
    return rot


# https://gist.github.com/angstwad/bf22d1822c38a92ec0a9
def dict_merge(dct, merge_dct):
    """ Recursive dict merge. Inspired by :meth:``dict.update()``, instead of
    updating only top-level keys, dict_merge recurses down into dicts nested
    to an arbitrary depth, updating keys. The ``merge_dct`` is merged into
    ``dct``.

    :param dct: dict onto which the merge is executed
    :param merge_dct: dct merged into dct
    :return: None
    """
    for k, v in merge_dct.items():
        if (k in dct and isinstance(dct[k], dict)
                and isinstance(merge_dct[k], collections.Mapping)):
            dict_merge(dct[k], merge_dct[k])
        else:
            dct[k] = merge_dct[k]


def setup_sensor(sensor, cfg):
    sensor.timeout = 3
    sensor.kSetAcqParams(polling=0,
                         flush=0,
                         acq_interval=0,
                         resp_interval=0)
    sensor.kSetDataComponents(
        id=['kTemperature', 'kDistortion',
            'kHeading', 'kPAngle', 'kRAngle'], count=5)
    if 'orientation' in cfg:
        sensor.kSetConfig(id='kMountingRef',
                          value=cfg['orientation'])


@click.command()
@click.option('--config', help='Configuration file', metavar='FILENAME')
def main(config):
    """
    gRPC server for the COVIS rotator system.
    """
    cfg = toml.loads(DEFAULT)
    if config:
        usercfg = toml.load(config)
        dict_merge(cfg, usercfg)

    # Initialize each rotator
    d = cfg['rotators'].get('roll')
    if d and ('device' in d) and d['device'] != '<mock>':
        try:
            roll = Rotator(serial.Serial(d['device'], d['baud'],
                                         timeout=d.get('timeout', 3)))
        except Exception:
            logging.exception('Roll rotator not accessible')
            roll = mock_rotator()
        else:
            roll.accel = d.get('accel', 2)
            roll.maxvel = d.get('maxvel', 1)
            roll.brake = d.get('brake', 89)
            if 'limits' in d:
                roll.cw = d['limits']['cw']
                roll.ccw = d['limits']['ccw']
    else:
        logging.warn('No roll rotator')
        roll = mock_rotator()

    d = cfg['rotators'].get('tilt')
    if d and ('device' in d) and d['device'] != '<mock>':
        try:
            tilt = Rotator(serial.Serial(d['device'], d['baud'],
                                         timeout=d.get('timeout', 3)))
        except Exception:
            logging.exception('Tilt rotator not accessible')
            tilt = mock_rotator()
        else:
            tilt.accel = d.get('accel', 2)
            tilt.maxvel = d.get('maxvel', 1)
            tilt.brake = d.get('brake', 89)
            if 'limits' in d:
                tilt.cw = d['limits']['cw']
                tilt.ccw = d['limits']['ccw']
    else:
        logging.warn('No tilt rotator')
        tilt = mock_rotator()

    d = cfg['rotators'].get('pan')
    if d and ('device' in d) and d['device'] != '<mock>':
        try:
            pan = Rotator(serial.Serial(d['device'], d['baud'],
                                        timeout=d.get('timeout', 3)))
        except Exception:
            logging.exception('Pan rotator not accessible')
            pan = mock_rotator()
        else:
            pan.accel = d.get('accel', 2)
            pan.maxvel = d.get('maxvel', 1)
            pan.brake = d.get('brake', 89)
            if 'limits' in d:
                pan.cw = d['limits']['cw']
                pan.ccw = d['limits']['ccw']
    else:
        logging.warn('No pan rotator')
        pan = mock_rotator()

    # Initialize the attitude sensor
    d = cfg.get('sensor')
    if d and 'device' in d:
        sensor = Tcm6(serial.Serial(d['device'], d['baud']),
                      timeout=d.get('timeout', 1))
        try:
            sensor.kGetModInfo()
            setup_sensor(sensor, d)
        except IOError:
            try:
                sensor.kGetModInfo()
                setup_sensor(sensor, d)
            except IOError:
                logging.warn('Cannot access attitude sensor')
                sensor = None

    else:
        logging.warn('No attitude sensor defined')
        sensor = None

    setup_logging()
    svc = create_server(pan, tilt, roll, sensor, cfg['address'])
    svc.start()
    try:
        signal.pause()
    finally:
        logging.info('Stopping')
        event = svc.stop(grace=0)
        logging.info('Server stop: %s', event)
        event.wait()
        logging.info('Stopped')
