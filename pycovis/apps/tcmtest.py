#!/usr/bin/env python
# -*- coding: utf-8 -*-
import click
import serial
import cmd2
import sys
import os
import termios
import tty
from select import select
from contextlib import contextmanager
from pycovis.tcm import Tcm6


def format_tcm_data(payload):
    """
    Pretty print the TCM data record.

    :param payload: data record
    :type payload: construct.Container
    :return: string
    """
    names = (('kPAngle', 'pitch'),
             ('kRAngle', 'roll'),
             ('kHeading', 'heading'))
    buf = []
    for k, v in names:
        buf.append('{0}={1:.2f}'.format(v, getattr(payload, k)))
    return ' '.join(buf)


def format_eng_data(payload):
    """
    Pretty print the TCM data record.

    :param payload: data record
    :type payload: construct.Container
    :return: string
    """
    dist = payload.kDistortion > 0 and 'TRUE' or 'FALSE'
    return 'temp={0:.1f} distortion={1}'.format(payload.kTemperature, dist)


def check_for_input(infile=sys.stdin):
    """
    Poll for an input character.

    :param infile: input file
    :return: character
    :platform: Unix
    """
    rlist, _, _ = select([infile], [], [], 0)
    if rlist:
        return os.read(infile.fileno(), 1)
    else:
        return None


@contextmanager
def cbreak_mode():
    """
    Place standard input into cbreak mode so we can read one
    character at a time.

    :platform: Unix
    """
    fd = sys.stdin.fileno()
    settings = termios.tcgetattr(fd)
    tty.setcbreak(fd)
    try:
        yield
    finally:
        termios.tcsetattr(fd, termios.TCSADRAIN, settings)


class TcmApp(cmd2.Cmd):
    """
    Simple command interpreter for testing a TCM

    Commands (type help <topic>)
    ==============================================
    read stream orientation info sleep wakeup
    cal restore quit
    """
    prompt = 'TCM> '
    tcm = None
    orientations = [
        ('std_0', 'Standard 0 deg (std_0)'),
        ('std_90', 'Standard 90 deg (std_90)'),
        ('std_180', 'Standard 180 deg (std_180)'),
        ('std_270', 'Standard 270 deg (std_270)'),
        ('xup_0', 'X-up 0 deg (xup_0)'),
        ('xup_90', 'X-up 90 deg (xup_90)'),
        ('xup_180', 'X-up 180 deg (xup_180)'),
        ('xup_270', 'X-up 270 deg (xup_270)'),
        ('yup_0', 'Y-up 0 deg (yup_0)'),
        ('yup_90', 'Y-up 90 deg (yup_90)'),
        ('yup_180', 'Y-up 180 deg (yup_180)'),
        ('yup_270', 'Y-up 270 deg (yup_270)'),
        ('zdown_0', 'Z-down 0 deg (zdown_0)'),
        ('zdown_90', 'Z-down 90 deg (zdown_90)'),
        ('zdown_180', 'Z-down 180 deg (zdown_180)'),
        ('zdown_270', 'Z-down 270 deg (zdown_270)')
    ]
    calibrations = [
        ('cal_full_range', 'Full Range Calibration'),
        ('cal_2d', '2D Calibration'),
        ('cal_hi', 'Hard Iron Only Calibration'),
        ('cal_limit_tilt', 'Limited Tilt Range Calibration'),
        (None, 'ABORT calibration')
    ]

    def postcmd(self, stop, line):
        if self.tcm.mode == 'sleep':
            self.prompt = 'TCM(sleep)> '
        else:
            self.prompt = 'TCM> '
        return stop

    def do_help(self, arg):
        if arg:
            cmd2.Cmd.do_help(self, arg)
        else:
            self.poutput(self.__doc__ + '\n')

    def do_read(self, arg):
        """read: read a sample from the TCM."""
        result = self.tcm.kGetData()
        if result and result.id == 'kDataResp':
            self.poutput(
                format_tcm_data(result.payload) +
                ' ' + format_eng_data(result.payload) + '\n')
        else:
            self.perror('Invalid response from device\n')

    def do_stream(self, arg):
        """stream: read data samples continuously."""
        resp = self.tcm.kSetAcqParams(polling=0,
                                      flush=0,
                                      acq_interval=0,
                                      resp_interval=0)
        if not (resp and resp.id == 'kAcqParamsDone'):
            self.perror('Invalid response from device\n')
            return
        self.pfeedback('Press any key to stop ...\n')
        self.tcm.kStartContinuousMode()
        with cbreak_mode():
            for ts, result in self.tcm:
                self.poutput(
                    format_tcm_data(result.payload) +
                    ' ' + format_eng_data(result.payload) + '\n')
                c = check_for_input()
                if c is not None:
                    break
        self.tcm.kStopContinuousMode()
        self.tcm.stop_reader()
        self.tcm.start_reader()
        self.tcm.kSetAcqParams(polling=0,
                               flush=1,
                               acq_interval=0,
                               resp_interval=0)

    def do_orientation(self, arg):
        """orientation [VALUE]: set the TCM mounting reference.

            if VALUE is omitted, a menu of valid values is displayed.
            if VALUE == ? show the current setting.
        """
        if not arg:
            self.pfeedback(
                self.colorize('See Fig 4-1 in TCM User Manual for diagrams',
                              'bold'))
            arg = self.select(self.orientations,
                              prompt='Select TCM orientation: ')
        keys = [e[0] for e in self.orientations]
        if arg in keys:
            self.tcm.kSetConfig(id='kMountingRef', value=arg)
        resp = self.tcm.kGetConfig(id='kMountingRef')
        self.poutput('Orientation: {0}\n'.format(resp.payload.kMountingRef))

    def do_info(self, arg):
        """info: return TCM device type/firmware revision"""
        resp = self.tcm.kGetModInfo()
        self.poutput('Type/rev: {0}/{1}\n'.format(
                     resp.payload.type, resp.payload.revision))

    def do_sleep(self, arg):
        """sleep: put device in power-down mode"""
        result = self.tcm.kPowerDown()
        if result.id != 'kPowerDownDone':
            self.perror('Command failed\n')

    def do_wakeup(self, arg):
        """wakeup: end power-down mode."""
        self.tcm.wakeup()
        result = self.tcm.next()
        if result.id != 'kPowerUp':
            self.perror('Command failed\n')

    def do_cal(self, arg):
        """cal NPOINTS: perform a magnetic calibration"""
        try:
            np = int(arg, 0)
        except ValueError:
            self.perror('You must specify a sample count\n')
            return
        if 4 <= np <= 32:
            self.tcm.kSetConfig(id='kUserCalNumPoints', value=np)
            self.tcm.kSetConfig(id='kUserCalAutoSampling', value=1)
            cal_type = self.select(self.calibrations,
                                   prompt='Select cal type: ')
            if cal_type is None:
                return
            self.poutput('Begin slowly moving sensor in a circle\n')
            self.tcm.kStartCal(type=cal_type)
            done = False
            while not done:
                for result in self.tcm:
                    if result.id == 'kUserCalSampCount':
                        self.poutput(
                            '{0} samples taken\n'.format(result.payload.count))
                    elif result.id == 'kDataResp':
                        self.poutput(format_tcm_data(result.payload) + '\n')
                    elif result.id == 'kUserCalScore':
                        score = result.payload.mag_score
                        self.poutput('CalScore = {0:.2f}\n'.format(score))
                        if score <= 2:
                            ans = raw_input('Save calibration [y/N]? ')
                            if ans in ('y', 'Y'):
                                self.tcm.kSave()
                        else:
                            self.perror('Invalid calibration')
                        done = True
                        self.tcm.kSetDataComponents(
                            id=['kTemperature', 'kDistortion',
                                'kHeading', 'kPAngle', 'kRAngle'], count=5)
                        break
        else:
            self.perror('Invalid number of cal points\n')

    def do_restore(self, args):
        """restore: reload the factory calibrations"""
        self.tcm.kFactoryUserCal()
        self.tcm.kSave()
        self.tcm.kFactoryInclCal()
        self.tcm.kSave()

    def emptyline(self):
        pass


@click.command()
@click.option('-b', '--baud', default=38400, type=int,
              help='set serial baud rate')
@click.argument('device')
@click.argument('commands', nargs=-1)
def cli(baud, device, commands):
    """
    Interactive test program for a TCM attitude sensor. DEVICE is the
    serial port that the sensor is connected to.
    """
    port = serial.Serial(device, baud)
    obj = TcmApp()
    obj.tcm = Tcm6(port, timeout=1)
    try:
        # Sometimes the first command fails if the device
        # is in a "weird state".
        _ = obj.tcm.kGetModInfo()
    except IOError:
        pass
    obj.tcm.timeout = 3
    obj.tcm.kSetAcqParams(polling=0,
                          flush=1,
                          acq_interval=0,
                          resp_interval=0)
    obj.tcm.kSetDataComponents(
        id=['kTemperature', 'kDistortion',
            'kHeading', 'kPAngle', 'kRAngle'], count=5)
    obj.echo = False
    obj.cmdqueue.extend(commands)
    obj._cmdloop()
