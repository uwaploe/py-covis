#!/usr/bin/env python
# -*- coding: utf-8 -*-
import traceback
import cmd2
import shlex
import time
import sys
import signal
from ast import literal_eval
from functools import partial
import serial
import click
from pycovis.ros import Rotator


def parse_cmdline(line):
    """
    Parse a command line into a list of arguments and a dictionary of
    variable assignments, akin to the ``*args`` and ``**kwds`` arguments
    to Python functions.

    >>> args, kwds = parse_cmdline('x 42 foo=bar baz=1,2,3')
    >>> args
    ['x', 42]
    >>> d = kwds.items()
    >>> d.sort()
    >>> d
    [('baz', [1, 2, 3]), ('foo', 'bar')]

    """
    def maybe_eval(s):
        try:
            return literal_eval(s)
        except ValueError:
            return s
    params = {}
    args = []
    for arg in shlex.split(line):
        try:
            name, val = arg.split('=')
            if ',' in val:
                params[name] = [maybe_eval(v) for v in val.split(',')]
            else:
                params[name] = maybe_eval(val)
        except ValueError:
            args.append(maybe_eval(arg))
    return args, params


class RotatorStall(Exception):
    pass


class RotatorTimeout(Exception):
    pass


def sig_handler(signum, *args):
    if signum != signal.SIGALRM:
        sys.exit(1)


def rotator_wait(rot, t_limit, interval=0.5, foutput=None):
    """
    Monitor the Rotator movement.
    """
    signal.signal(signal.SIGALRM, sig_handler)
    signal.setitimer(signal.ITIMER_REAL, interval, interval)
    try:
        active = len(rot)
        while active > 0 and time.time() < t_limit:
            signal.pause()
            for k, v in rot.items():
                status = v.flags
                if (status & 1) == 0:
                    active = active - 1
                if foutput:
                    foutput('{} angle={:.1f} status={:02x}'.format(k, v.angle, status))
    finally:
        signal.setitimer(signal.ITIMER_REAL, 0)


class RosApp(cmd2.Cmd):
    """
    Simple command interpreter for testing multiple Rotators.
    Command arguments take the form AXIS=value where AXIS is
    either 'pan', 'tilt', or 'roll'.

    Commands (type help <command>)
    ===============================================
    moveto move where limits brake accel speed
    steps reset stepto (experimental)
    quit
    """
    prompt = 'ROS> '
    rot = None

    def _output(self, msg, color=None):
        if color:
            self.poutput(self.colorize(msg, color))
        else:
            self.poutput(msg)

    def do_where(self, line):
        """where: show current rotator position(s)"""
        assert self.rot is not None
        for k, v in self.rot.items():
            self._output('{}={:.1f}'.format(k, v.angle), 'green')

    def do_steps(self, line):
        """where: show current rotator step counter(s)"""
        assert self.rot is not None
        for k, v in self.rot.items():
            self._output('{}={:.1f}'.format(k, v.counter), 'green')

    def do_brake(self, line):
        """brake [AXIS=value ...]: get/set the braking value(s)"""
        assert self.rot is not None
        args, kwds = parse_cmdline(line)
        if kwds:
            for k, v in kwds.items():
                self.rot[k].brake = int(v)
        else:
            for k, v in self.rot.items():
                self._output('{}={:d}'.format(k, v.brake), 'green')

    def do_accel(self, line):
        """accel [AXIS=deg/s^2 ...]: get/set the rotator acceleration value(s)"""
        assert self.rot is not None
        args, kwds = parse_cmdline(line)
        if kwds:
            for k, v in kwds.items():
                self.rot[k].accel = v
        else:
            for k, v in self.rot.items():
                self._output('{}={:.1f}'.format(k, v.accel), 'green')

    def do_speed(self, line):
        """speed [deg/s]: get/set the default speed"""
        assert self.rot is not None
        args, kwds = parse_cmdline(line)
        if kwds:
            for k, v in kwds.items():
                self.rot[k].maxvel = v
        else:
            for k, v in self.rot.items():
                self._output('{}={:.1f}'.format(k, v.maxvel), 'green')

    def do_moveto(self, line):
        """moveto AXIS=DEG [speed=DEG/s]: move to an absolute position"""
        assert self.rot is not None
        args, params = parse_cmdline(line)
        speed = params.get('speed', 0)
        try:
            del params['speed']
        except KeyError:
            pass

        try:
            rot = {}
            if speed > 0:
                for k, v in params.items():
                    self.rot[k].maxvel = speed
            t0 = time.time()
            for k, v in params.items():
                rot[k] = self.rot[k]
                dt = self.rot[k].moveto(v)
            if dt > 0:
                rotator_wait(rot, t0 + dt*2.5,
                             foutput=partial(self._output,
                                             color='green'))
                self._output('Elapsed time: {:.1f}s ({:.1f}s)'.format(
                    time.time() - t0,
                    dt))
        except Exception as e:
            self._output(str(e), 'red')
            tb = sys.exc_info()[2]
            traceback.print_tb(tb)

    def do_move(self, line):
        """move AXIS=DEG [speed=DEG/s]: move to a relative position"""
        assert self.rot is not None
        args, params = parse_cmdline(line)
        speed = params.get('speed', 0)
        try:
            del params['speed']
        except KeyError:
            pass

        try:
            rot = {}
            if speed > 0:
                for k, v in params.items():
                    self.rot[k].maxvel = speed
            t0 = time.time()
            for k, v in params.items():
                rot[k] = self.rot[k]
                dt = self.rot[k].move(v)
            if dt > 0:
                flags = rotator_wait(rot, t0 + dt*2.5,
                                     foutput=partial(self._output,
                                                     color='green'))
                self._output('Elapsed time: {:.1f}s ({:.1f}s)'.format(
                    time.time() - t0,
                    dt))
        except Exception as e:
            self._output(str(e), 'red')
            tb = sys.exc_info()[2]
            traceback.print_tb(tb)

    def do_stepto(self, line):
        """stepto AXIS=DEG [speed=DEG/s]: step to an absolute position"""
        assert self.rot is not None
        args, params = parse_cmdline(line)
        speed = params.get('speed', 0)
        try:
            del params['speed']
        except KeyError:
            pass

        try:
            rot = {}
            t0 = time.time()
            for k, v in params.items():
                rot[k] = self.rot[k]
                dt = self.rot[k].stepto(v, speed or self.rot[k].maxvel)
            if dt > 0:
                rotator_wait(rot, t0 + dt*2.5,
                             foutput=partial(self._output,
                                             color='green'))
                self._output('Elapsed time: {:.1f}s ({:.1f}s)'.format(
                    time.time() - t0,
                    dt))
        except Exception as e:
            self._output(str(e), 'red')
            tb = sys.exc_info()[2]
            traceback.print_tb(tb)

    def do_step(self, line):
        """step AXIS=DEG [speed=DEG/s]: step to a relative position"""
        assert self.rot is not None
        args, params = parse_cmdline(line)
        speed = params.get('speed', 0)
        try:
            del params['speed']
        except KeyError:
            pass

        try:
            rot = {}
            t0 = time.time()
            for k, v in params.items():
                rot[k] = self.rot[k]
                dt = self.rot[k].step(v, speed or self.rot[k].maxvel)
            if dt > 0:
                rotator_wait(rot, t0 + dt*2.5,
                             foutput=partial(self._output,
                                             color='green'))
                self._output('Elapsed time: {:.1f}s ({:.1f}s)'.format(
                    time.time() - t0,
                    dt))
        except Exception as e:
            self._output(str(e), 'red')
            tb = sys.exc_info()[2]
            traceback.print_tb(tb)

    def do_reset(self, line):
        """reset: reset the step counter(s)"""
        assert self.rot is not None
        for k, v in self.rot.items():
            v.reset_counter()

    def do_limits(self, line):
        """limits [AXIS=DEG,DEG]: get/set user limits"""
        assert self.rot is not None
        args, params = parse_cmdline(line)
        for k, v in params.items():
            if v[0] >= 0:
                self.rot[k].ccw = v[0]
            if v[1] >= 0:
                self.rot[k].cw = v[1]
        for k, v in self.rot.items():
            self._output('{0} ccw={1.ccw:.0f} cw={1.cw:.0f}'.format(k, v))

    def do_load(self, args):
        """load FILENAME: execute commands from a file."""
        if not args:
            self.perror('Missing filename\n')
        else:
            name = args.strip()
            colors, self.colors = self.colors, False
            try:
                with open(name, 'r') as f:
                    for line in f:
                        if not line.startswith('#'):
                            line = self.precmd(line.strip())
                            if line:
                                self.poutput('>' + line + '\n')
                                stop = self.onecmd(line)
                                if stop:
                                    return True
            except IOError as e:
                self.perror(str(e) + '\n')
            finally:
                self.colors = colors

    def do_help(self, arg):
        if arg:
            cmd2.Cmd.do_help(self, arg)
        else:
            self.poutput(self.__doc__ + '\n')

    def emptyline(self):
        pass


@click.command()
@click.option('-b', '--baud', default=9600, type=int,
              help='set serial baud rate')
@click.option('-p', '--pan',
              metavar='DEVICE',
              envvar='ROS_PAN',
              help='serial device for PAN rotator')
@click.option('-t', '--tilt',
              metavar='DEVICE',
              envvar='ROS_TILT',
              help='serial device for TILT rotator')
@click.option('-r', '--roll',
              metavar='DEVICE',
              envvar='ROS_ROLL',
              help='serial device for ROLL rotator')
@click.argument('commands', nargs=-1)
def cli(baud, pan, tilt, roll, commands):
    """
    Interactive test program for an ROS Rotator. DEVICE is the serial
    port that the Rotator is connected to.
    """
    obj = RosApp()
    obj.rot = {}
    if pan:
        port = serial.Serial(pan, baud, timeout=3)
        obj.rot['pan'] = Rotator(port)
    if tilt:
        port = serial.Serial(tilt, baud, timeout=3)
        obj.rot['tilt'] = Rotator(port)
    if roll:
        port = serial.Serial(roll, baud, timeout=3)
        obj.rot['roll'] = Rotator(port)
    if len(obj.rot) == 0:
        raise SystemExit('No rotators specified')
    obj.echo = False
    try:
        obj.cmdqueue.extend(commands)
        obj._cmdloop()
    finally:
        for r in obj.rot.values():
            if r.is_moving:
                r.stop()
