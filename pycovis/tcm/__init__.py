#!/usr/bin/env python
#
from . import frames as F
import time
import array
from queue import Queue, Empty
from threading import Thread, Event


class BadCrc(Exception):
    """
    Exception raised on CRC check failure.

    :var sent: CRC included in the packet.
    :var calc: CRC calculated from packet contents.
    """
    def __init__(self, sent, calc):
        self.sent = sent
        self.calc = calc

    def __str__(self):
        return 'sent={0:04x} calc={1:04x}'.format(self.sent, self.calc)


class Tcm6(object):
    """
    Class to represent a TCM6, tilt-compensated compass module attached to a
    serial port. TCM6 commands are implemented as object methods.
    """
    def __init__(self, port, timeout=3):
        """
        Initialize a class instance.

        :param port: serial port interface
        """
        self.port = port
        self._timeout = timeout
        self.port.timeout = timeout
        self.ev = Event()
        self.mode = 'normal'
        self.task = None
        self.start_reader()

    def stop_reader(self):
        self.ev.set()
        self.task.join(self._timeout * 2)
        if self.task.is_alive():
            raise RuntimeError('Cannot stop reader thread')
        self.task = None

    def start_reader(self):
        if self.task:
            self.stop_reader()
        # Make sure the device is not in continuous mode
        packet = F.build_datagram(F.build_frame('kStopContinuousMode'))
        self.port.write(packet)
        self.ev.clear()
        self.queue = Queue()
        self.task = Thread(target=self._reader)
        self.task.daemon = True
        self.task.start()

    def _get_bytes(self, n):
        try:
            data = self.port.read(n)
        except IOError:
            data = ''
        return array.array('B', data)

    def _reader(self):
        while not self.ev.is_set():
            crc = 0
            try:
                msb, lsb = self._get_bytes(2)
                ts = time.time()
                crc = F.update_crc(lsb, F.update_crc(msb, crc))
                remaining = lsb + msb*256 - 2
                if remaining <= 0:
                    continue
                contents = self._get_bytes(remaining - 2)
                #crc = reduce(lambda a, b: F.update_crc(b, a), contents, crc)
                msb, lsb = self._get_bytes(2)
                check = lsb + msb*256
                crc = check
                if check != crc:
                    self.queue.put(BadCrc(check, crc))
                else:
                    frame = F.data_frame.parse(contents.tostring())
                    self.queue.put((ts, frame))
                    # Throw away the first byte received after power-up
                    if frame.id == 'kPowerDownDone':
                        self.port.read(1)
            except ValueError:
                pass

    def __iter__(self):
        return self

    @property
    def timeout(self):
        """
        Command timeout in seconds or ``None``
        """
        return self._timeout

    @timeout.setter
    def timeout(self, val):
        self._timeout = val

    def wakeup(self):
        """
        Send wakeup character to the device. This is only needed if the device
        is in SYNC mode. See section 7.3.34 of the manual.
        """
        self.port.write(b'\xff')
        time.sleep(7e-4)

    def next(self):
        """
        When used as a iterator, return the next data frame. This is only
        useful if the device is in 'push' (interval) sampling mode.
        """
        try:
            item = self.queue.get(timeout=self._timeout)
            if isinstance(item, Exception):
                raise item
            ts, frame = item
            if frame.id == 'kSetModeResp':
                self.mode = frame.payload.mode_id
            elif frame.id == 'kPowerDownDone':
                self.mode = 'sleep'
            else:
                self.mode = 'normal'
            return ts, F.reformat_payload(frame)
        except Empty:
            raise StopIteration

    def __getattr__(self, name):
        """
        Implement the TCM6 commands as methods. Command parameters are passed
        as keyword arguments. Command names match those listed in Table 7-2 of
        the manual. See command definitions in frames.py for the parameter
        names.

        Examples:
            obj.kSetConfig(id='kMountingRef', value='xup_180')
            obj.kGetModInfo()
        """
        def _send_command(**params):
            packet = F.build_datagram(F.build_frame(name, **params))
            self.port.write(packet)
            # Any character will wake the device from sleep
            if self.mode == 'sleep':
                self.mode = 'normal'
            if name in F.no_response:
                return None
            else:
                try:
                    ts, response = self.next()
                    return response
                except StopIteration:
                    raise IOError('No response from TCM')
        if self.mode == 'sync':
            self.wakeup()
        return _send_command


def test(devname):
    import serial
    port = serial.Serial(devname, 38400)
    tcm6 = Tcm6(port, timeout=3)
    print(tcm6.kGetModInfo())
    print(tcm6.kPowerDown())
    time.sleep(2)
    tcm6.wakeup()
    print(tcm6.next())
    print(tcm6.kGetData())
    port.close()

if __name__ == '__main__':
    test('/dev/ttyMI3')
