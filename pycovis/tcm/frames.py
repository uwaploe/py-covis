#!/usr/bin/env python
#
# TCM6 communication protocol data structures
#
import construct as C
from array import array

component_id = C.Enum(C.Byte('id'),
                      kHeading=5,
                      kTemperature=7,
                      kDistortion=8,
                      kCalStatus=9,
                      kPAligned=21,
                      kRAligned=22,
                      kIZAligned=23,
                      kPAngle=24,
                      kRAngle=25,
                      KXAligned=27,
                      KYAligned=28,
                      KZAligned=29)

config_id = C.Enum(C.Byte('id'),
                   kDeclination=1,
                   kTrueNorth=2,
                   kBigEndian=6,
                   kMountingRef=10,
                   kUserCalNumPoints=12,
                   kUserCalAutoSampling=13,
                   kBaudRate=14)

mounting_ref = C.Enum(C.Byte('mount'),
                      std_0=1,
                      xup_0=2,
                      yup_0=3,
                      std_90=4,
                      std_180=5,
                      std_270=6,
                      zdown_0=7,
                      xup_90=8,
                      xup_180=9,
                      xup_270=10,
                      yup_90=11,
                      yup_180=12,
                      yup_270=13,
                      zdown_90=14,
                      zdown_180=15,
                      zdown_270=16)

baud_rate = C.Enum(C.Byte('speed'),
                   b300=0,
                   b600=1,
                   b1200=2,
                   b1800=3,
                   b2400=4,
                   b3600=5,
                   b4800=6,
                   b7200=7,
                   b9600=8,
                   b14400=9,
                   b19200=10,
                   b28800=11,
                   b38400=12,
                   b57600=13,
                   b115200=14)

cal_type = C.Enum(C.UBInt32('type'),
                  cal_full_range=10,
                  cal_2d=20,
                  cal_hi=30,
                  cal_limit_tilt=40,
                  cal_accel_only=100,
                  cal_accel_mag=110)

data_value = C.Struct('pair',
                      component_id,
                      C.Switch('value',
                               lambda ctx: ctx['id'],
                               {'kHeading': C.BFloat32('heading'),
                                'kTemperature': C.BFloat32('temp'),
                                'kDistortion': C.UBInt8('isdistorted'),
                                'kCalStatus': C.UBInt8('iscal'),
                                'kPAligned': C.BFloat32('pitch_cal'),
                                'kRAligned': C.BFloat32('roll_cal'),
                                'kIZAligned': C.BFloat32('iz_cal'),
                                'kPAngle': C.BFloat32('pitch'),
                                'kRAngle': C.BFloat32('roll'),
                                'KXAligned': C.BFloat32('xmag_cal'),
                                'KYAligned': C.BFloat32('ymag_cal'),
                                'KZAligned': C.BFloat32('izmag_cal')}))

config_value = C.Struct('pair',
                        config_id,
                        C.Switch('value',
                                 lambda ctx: ctx['id'],
                                 {'kDeclination': C.BFloat32('decl'),
                                  'kTrueNorth': C.UBInt8('istrue'),
                                  'kBigEndian': C.UBInt8('isbigendian'),
                                  'kMountingRef': mounting_ref,
                                  'kUserCalNumPoints': C.UBInt32('points'),
                                  'kUserCalAutoSampling': C.UBInt8('autosample'),
                                  'kBaudRate': baud_rate}))

axis_id = C.Enum(C.UBInt8('axis_id'),
                 kXAxis=1,
                 kYAxis=2,
                 kZAxis=3)

mode_id = C.Enum(C.UBInt8('mode_id'),
                 normal=0,
                 sync=100)

filter_params = C.Struct('filterParams',
                         C.UBInt8('param_id'),
                         axis_id,
                         C.UBInt8('count'),
                         C.Array(lambda ctx: ctx['count'],
                                 C.BFloat64('value')))

acq_params = C.Struct('acqParams',
                      C.UBInt8('polling'),
                      C.UBInt8('flush'),
                      C.BFloat32('acq_interval'),
                      C.BFloat32('resp_interval'))

#: kModInfoResp (frame ID 2)
mod_info_resp = C.Struct('modInfoResp',
                         C.String('type', 4),
                         C.String('revision', 4))

#: kSetDataComponents (frame ID 3)
set_data_components = C.Struct('setDataComponents',
                               C.UBInt8('count'),
                               C.Array(lambda ctx: ctx['count'], component_id))


#: kDataResp (frame ID 5)
data_response = C.Struct('dataResponse',
                         C.UBInt8('count'),
                         C.Array(lambda ctx: ctx['count'], data_value))

#: kSetConfig (frame ID 6)
set_config = C.Rename('setConfig', config_value)

#: kGetConfig (frame ID 7)
get_config = C.Struct('getConfig', config_id)

#: kConfigResp (frame ID 8)
config_resp = C.Rename('configResp', config_value)

#: kStartCal (frame ID 10)
start_cal = C.Struct('startCal', cal_type)

#: kSetFIRFilters (frame ID 12)
set_param = C.Rename('setParam', filter_params)

#: kGetFIRFilters (frame ID 13)
get_param = C.Struct('getParam', C.UBInt8('param_id'), axis_id)

#: kGetFIRFiltersResp (frame ID 14)
param_resp = C.Rename('paramResp', filter_params)

#: kSaveDone (frame ID 16)
save_done = C.Struct('saveDone', C.UBInt16('error'))

#: kUserCalSampCount (frame ID 17)
cal_samp_count = C.Struct('userCalSampCount', C.UBInt32('count'))

#: kUserCalScore (frame ID 18)
cal_score = C.Struct('userCalScore',
                     C.BFloat32('mag_score'),
                     C.BFloat32('param2'),
                     C.BFloat32('accel_score'),
                     C.BFloat32('dist_err'),
                     C.BFloat32('tilt_err'),
                     C.BFloat32('tilt_range'))

#: kSetAcqParams (frame ID 24)
set_acq_params = C.Rename('setAcqParams', acq_params)

#: kAcqParamsResp (frame ID 27)
acq_params_resp = C.Rename('acqParamsResp', acq_params)

#: kSetMode (frame ID 46)
set_mode = C.Struct('setMode', mode_id)

#: kSetModeResp (frame ID 47)
set_mode_resp = C.Struct('setModeResp', mode_id)

#: Data frame
data_frame = C.Struct('frame',
                      C.Enum(C.Byte('id'),
                             kGetModInfo=1,
                             kModInfoResp=2,
                             kSetDataComponents=3,
                             kGetData=4,
                             kDataResp=5,
                             kSetConfig=6,
                             kGetConfig=7,
                             kConfigResp=8,
                             kSave=9,
                             kStartCal=10,
                             kStopCal=11,
                             kSetFIRFilters=12,
                             kGetFIRFilters=13,
                             kGetFIRFiltersResp=14,
                             kPowerDown=15,
                             kSaveDone=16,
                             kUserCalSampCount=17,
                             kUserCalScore=18,
                             kSetConfigDone=19,
                             kSetFIRFiltersDone=20,
                             kStartContinuousMode=21,
                             kStopContinuousMode=22,
                             kPowerUpDone=23,
                             kSetAcqParams=24,
                             kGetAcqParams=25,
                             kAcqParamsDone=26,
                             kAcqParamsResp=27,
                             kPowerDownDone=28,
                             kFactoryUserCal=29,
                             kFactoryUserCalDone=30,
                             kTakeUserCalSample=31,
                             kFactoryInclCal=36,
                             kFactoryInclCalDone=37,
                             kSetMode=46,
                             kSetModeResp=47,
                             kSyncRead=49),
                      C.Switch('payload',
                               lambda ctx: ctx['id'],
                               {'kGetModInfo': C.Pass,
                                'kModInfoResp': mod_info_resp,
                                'kSetDataComponents': set_data_components,
                                'kGetData': C.Pass,
                                'kDataResp': data_response,
                                'kSetConfig': set_config,
                                'kGetConfig': get_config,
                                'kConfigResp': config_resp,
                                'kSave': C.Pass,
                                'kStartCal': start_cal,
                                'kStopCal': C.Pass,
                                'kSetFIRFilters': set_param,
                                'kGetFIRFilters': get_param,
                                'kGetFIRFiltersResp': param_resp,
                                'kPowerDown': C.Pass,
                                'kSaveDone': save_done,
                                'kUserCalSampCount': cal_samp_count,
                                'kUserCalScore': cal_score,
                                'kSetConfigDone': C.Pass,
                                'kSetFIRFiltersDone': C.Pass,
                                'kStartContinuousMode': C.Pass,
                                'kStopContinuousMode': C.Pass,
                                'kPowerUpDone': C.Pass,
                                'kSetAcqParams': set_acq_params,
                                'kGetAcqParams': C.Pass,
                                'kAcqParamsDone': C.Pass,
                                'kAcqParamsResp': acq_params_resp,
                                'kPowerDownDone': C.Pass,
                                'kFactoryUserCal': C.Pass,
                                'kFactoryUserCalDone': C.Pass,
                                'kTakeUserCalSample': C.Pass,
                                'kFactoryInclCal': C.Pass,
                                'kFactoryInclCalDone': C.Pass,
                                'kSetMode': set_mode,
                                'kSetModeResp': set_mode_resp,
                                'kSyncRead': C.Pass}))

#: Commands from which there is no response
no_response = ('kStartCal', 'kStopCal', 'kSetDataComponents',
               'kStartContinuousMode', 'kStopContinuousMode')

datagram = C.Struct('datagram',
                    C.UBInt16('count'),
                    C.Field('contents', lambda ctx: ctx['count'] - 4),
                    C.UBInt16('crc'))


def reformat_payload(frame):
    """
    Reformat the payload attribute of the following frame types:

    kDataResp
    kConfigResp

    >>> f = C.Container(id = 'kDataResp', payload = C.Container(count = 3, pair = [C.Container(id = 'kHeading', value = 333.0), C.Container(id = 'kPAngle', value = -15.0), C.Container(id = 'kRAngle', value = -0.5)]))
    >>> reformat_payload(f)
    Container({'payload': Container({'kRAngle': -0.5, 'kPAngle': -15.0, 'kHeading': 333.0}), 'id': 'kDataResp'})
    >>> f = C.Container(id = 'kConfigResp', payload = C.Container(id = 'kMountingRef', value = 'std_0'))
    >>> reformat_payload(f)
    Container({'payload': Container({'kMountingRef': 'std_0'}), 'id': 'kConfigResp'})
    """
    if frame.id == 'kDataResp':
        values = {}
        for container in frame.payload.pair:
            values[container.id] = container.value
        frame.payload = C.Container(**values)
    elif frame.id == 'kConfigResp':
        values = {frame.payload.id: frame.payload.value}
        frame.payload = C.Container(**values)
    return frame


def build_frame(frame_type, **kwds):
    r"""
    Construct a new data frame.

    :param frame_type: frame type name
    :type frame_type: string
    :param kwds: parameter name/value pairs
    :return: frame contents as a string

    >>> build_frame('kGetModInfo')
    '\x01'
    >>> build_frame('kSetConfig', id='kTrueNorth', value=1)
    '\x06\x02\x01'
    >>> build_frame('kSetConfig', id='kMountingRef', value='xup_180')
    '\x06\n\t'
    >>> build_frame('kSetMode', mode_id='normal')
    '.\x00'
    >>> build_frame('kSetMode', mode_id='sync')
    '.d'
    >>> build_frame('kSetAcqParams', polling=1, flush=0, acq_interval=0, resp_interval=0)
    '\x18\x01\x00\x00\x00\x00\x00\x00\x00\x00\x00'
    """
    if kwds:
        payload = C.Container(**kwds)
    else:
        payload = None
    return data_frame.build(C.Container(id=frame_type,
                                        payload=payload))


def update_crc(newval, crc=0):
    crc = ((crc << 8) | ((crc >> 8) & 0xff))
    crc = crc ^ newval
    crc = crc ^ ((crc & 0xff) >> 4)
    crc = crc ^ ((crc << 8) << 4)
    crc = crc ^ (((crc & 0xff) << 4) << 1)
    return crc & 0xffff


def build_datagram(contents):
    r"""
    Construct a datagram (packet)

    :param contents: data frame contents
    :type contents: string
    :return: datagram contents as a string.

    >>> build_datagram(build_frame('kGetModInfo'))
    '\x00\x05\x01\xef\xd4'
    """
    def calc_crc(bytes):
        c = 0
        for byte in bytes:
            c = update_crc(byte, c)
        return c
    count = len(contents) + 4
    # CRC calculation includes the Datagram Size field
    buf = array('B', [(count << 8) & 0xff, count & 0xff])
    buf.fromstring(contents)
    crc = calc_crc(buf)
    return datagram.build(C.Container(count=count,
                                      contents=contents,
                                      crc=crc))

__all__ = [
    'reformat_payload',
    'build_frame',
    'build_datagram',
    'no_response',
    'update_crc'
]


if __name__ == "__main__":
    import doctest
    doctest.testmod()
