#!/bin/sh

# Power cycle the attitude sensor and rotators
covis-power -off v24 sensor pan tilt roll
sleep 1
covis-power -on v24
# The environment variable NO_${d} can be used to
# disable the sensor named "$d"
for d in sensor pan tilt roll; do
    if eval test -z \$NO_${d}; then
        covis-power -on $d
        sleep 1
    fi
done
sleep 2

exec "$@"
