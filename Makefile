DATE    ?= $(shell date +%FT%T%z)
VERSION ?= $(shell git describe --tags --always --dirty --match=v* 2> /dev/null || \
			cat $(CURDIR)/.version 2> /dev/null || echo v0)
VCS_REF := $(shell git log -1 --pretty=%h)

NAME := pycovis
HOST ?= sysop@covis-sic-1

all: image

image: Dockerfile entrypoint.sh
	docker build -t $(NAME):latest --build-arg VERSION="${VERSION}" \
	--build-arg BUILD_DATE="${DATE}" \
	--build-arg VCS_REF="${VCS_REF}" .

dist:
	docker save $(NAME):latest | bzip2 | ssh $(HOST) docker load
