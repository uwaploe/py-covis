FROM python:3 as builder
COPY req.txt .
RUN pip install -r req.txt

FROM python:3-slim
COPY --from=builder /root/.cache /root/.cache
COPY --from=builder req.txt .
RUN pip install -r req.txt && rm -rf /root/.cache
RUN mkdir -p /src/pycovis
COPY setup.py /src/
COPY pycovis /src/pycovis/
RUN pip install -e /src

ENV DAQ1278_VERS 0.9.0
ENV DAQ1278_REV 9a68bed8e2667f058f5d3c4ea5825202b8300779
ENV DAQ1278_SERVER=172.17.0.1:10000

RUN apt-get update && apt-get install -y --no-install-recommends wget \
&& wget -O /usr/bin/daq1278_client "https://bitbucket.org/uwaploe/daq1278/downloads/daq1278_client-v${DAQ1278_VERS}" \
&& chmod a+x /usr/bin/daq1278_client \
&& wget -O /usr/bin/covis-power "https://bitbucket.org/uwaploe/daq1278/raw/$DAQ1278_REV/extras/covis-power.sh" \
&& chmod a+x /usr/bin/covis-power
COPY entrypoint.sh /

EXPOSE 10130

ARG VERSION
ARG BUILD_DATE
ARG VCS_REF

LABEL org.label-schema.build-date=$BUILD_DATE \
      org.label-schema.version=$VERSION \
      org.label-schema.vcs-ref=$VCS_REF

ENTRYPOINT ["/entrypoint.sh"]
CMD ["rostest"]
