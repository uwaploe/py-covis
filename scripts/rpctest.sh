#!/usr/bin/env bash

: ${IMAGE="pycovis:latest"}

CFGFILE=rpc_cfg.$$.toml
trap "rm -f $CFGFILE; exit 1" 0 1 2 3 15

cat <<EOF > $CFGFILE
address = "[::]:10130"
[rotators.roll]
device = "/dev/ttyMI10"
baud = 9600
[rotators.tilt]
device = "/dev/ttyMI8"
baud = 9600
[rotators.pan]
device = "/dev/ttyMI9"
baud = 9600
[sensor]
device = "/dev/ttyMI5"
baud = 38400
orientation = "std_270"
EOF

covis-power -on v24 pan tilt roll sensor
docker run --rm -it \
       --device=/dev/ttyMI8:/dev/ttyMI8 \
       --device=/dev/ttyMI9:/dev/ttyMI9 \
       --device=/dev/ttyMI10:/dev/ttyMI10 \
       --device=/dev/ttyMI5:/dev/ttyMI5 \
       -v $PWD:/config \
       -p 10130:10130 \
       $IMAGE rotatorsvc --config /config/$CFGFILE
covis-power -off v24 pan tilt roll sensor