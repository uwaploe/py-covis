#!/usr/bin/env bash

: ${IMAGE="pycovis:latest"}

covis-power -on v24 pan tilt roll
docker run --rm -it \
       --device=/dev/ttyMI8:/dev/ttyMI8 \
       --device=/dev/ttyMI9:/dev/ttyMI9 \
       --device=/dev/ttyMI10:/dev/ttyMI10 \
       -e ROS_TILT=/dev/ttyMI8 \
       -e ROS_ROLL=/dev/ttyMI10 \
       -e ROS_PAN=/dev/ttyMI9 \
       $IMAGE rostest
covis-power -off v24 pan tilt roll
